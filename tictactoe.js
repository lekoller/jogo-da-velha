let currentPlayer = 'x';
let nextPlayer = 'o';

let playerXSelections = [];
let playerOSelections = [];
let playerSelections = [];
 
const winningCombinations = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
];

const cells = document.querySelectorAll('td');
const dial = document.getElementById('dialogo');
const stat = document.getElementById('statistics');
const button = document.createElement('div');
const text = document.createElement('p');

const handleClick = function(event) {
    const cell = event.target;
    if(cell.innerHTML === '') {
        cell.innerHTML = currentPlayer;

        if (currentPlayer === 'x' ) {
            dial.innerHTML = 'Jogador 2 - ready?'
            playerSelections = playerXSelections;
            nextPlayer = 'o';
        } else {
            dial.innerHTML = 'Jogador 1 - ready?'
            playerSelections = playerOSelections;
            nextPlayer = 'x';
        }

        playerSelections.push(Number(cell.id));

        if(checkWinner()) {
            dial.innerHTML = 'Temos um vencedor!';
            resetButton();
        } else if(checkDraw()) {
            dial.innerHTML = 'Empate...';
            resetButton();
        }

        // Swap players
        currentPlayer = nextPlayer;
        console.log(cell.id);
    }
}

for (let i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', handleClick);
}

function checkWinner() {
    
    let output = false;

    function master(valor) {
        let matches = 0;
        for(let i = 0; i < valor.length; i++) {
            for(let j = 0; j < playerSelections.length; j++) {
                if(valor[i] === playerSelections[j]){
                    matches++;
                }
                if(matches === 3) {
                    output = true;
                }
            }
        }
    }

    winningCombinations.forEach(master);
    return output;
 }

function checkDraw() {
    return (playerOSelections.length + playerXSelections.length) >= cells.length;
}

const resetGame = function() {
    
    playerXSelections = new Array();
    playerOSelections = new Array();
    for(let i = 0; i < cells.length; i++) {
        cells[i].innerHTML = '';
    }
    button.removeChild(text);
    stat.removeChild(button);
    dial.innerHTML = 'Jogador 1 - ready?';
}

function resetButton() {
    stat.appendChild(button);
    button.id = 'reset';
    button.appendChild(text);
    text.innerText = 'novo jogo?';
    text.id = 'texto';

    button.addEventListener('click', resetGame);
}
